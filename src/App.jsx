import React, { useState } from "react";
import movies from "./assets/movies.json";
import SetStatus from "./SetStatus";
const App = () => {
  
  return (
    <>
      <div>
        <SetStatus movies={movies} />
      </div>
    </>
  );
};

export default App;
