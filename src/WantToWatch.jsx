import React, { useState } from 'react'
import StatusSelector from './StatusSelector';

const WantToWatch = ({data}) => {
    const [want,setWant] = useState(data);
    data = data.filter((ele) => {
        if(ele.status === 'toWatch'){
            return true;
        }
        else{
            return false;
        }
    })
  return (<>
    {data.map((ele) => {
        return (<div>{ele.Title} <StatusSelector ele={ele}/></div> )
    })}
    </>
  )
}

export default WantToWatch