import React, { useState } from "react";
import WantToWatch from "./WantToWatch";
import Watching from "./Watching";
import WillWatch from "./WillWatch";

const SetStatus = ({ movies }) => {
  let data = movies;
  data = data.map((ele) => {
    ele = { ...ele, status: "toWatch" };
    return ele;
  });
  console.log(data);
  return (
    <>
    <div>
        <h1>Want to watch</h1>
        <hr></hr>
        <WantToWatch data={data} />
        <h1>Watching</h1>
        <hr></hr>
        <Watching data={data}/>
        <h1>Will Watch</h1>
        <hr></hr>
        <WillWatch data={data}/>
    </div>
    </>
  );
};

export default SetStatus;
