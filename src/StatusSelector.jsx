import React, { useState } from "react";

const StatusSelector = ({ ele, data }) => {
  function handleChange(event) {
    setSelected(event.target.value);
    data = data.map((element) => {
      if (element.rank == eleRank) {
        element.status = event.target.value;
      }
    });
  }
  const [selected, setSelected] = useState(ele.status);
  return (
    <>
      <select value={selected} onChange={handleChange}>
        <option value="toWatch">To Watch</option>

        <option value="watching">Watching</option>

        <option value="willWatch">Will Watch</option>
      </select>
    </>
  );
};

export default StatusSelector;
